package com.example.gamebaucua;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    Button Play;
    ImageView hinh0, h1, h2, h3;
    ArrayList<Integer> mang;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Play = (Button) findViewById(R.id.btnPlay);
        hinh0 = (ImageView) findViewById(R.id.imageViewChu);
        h1 = (ImageView) findViewById(R.id.imageView1);
        h2 = (ImageView) findViewById(R.id.imageView2);
        h3 = (ImageView) findViewById(R.id.imageView3);

        mang = new ArrayList<Integer>();
        mang.add(R.drawable.hinh1);
        mang.add(R.drawable.hinh2);
        mang.add(R.drawable.hinh3);
        mang.add(R.drawable.hinh4);
        mang.add(R.drawable.hinh5);
        mang.add(R.drawable.hinh6);
       hinh0.setImageResource(R.drawable.chu);
        Play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Random rd = new Random();
                int n = rd.nextInt(mang.size());
                h1.setImageResource(mang.get(n));

                int j = rd.nextInt(mang.size());
                h2.setImageResource(mang.get(j));

                int d = rd.nextInt(mang.size());
                h3.setImageResource(mang.get(d));

            }
        });
    }
}
